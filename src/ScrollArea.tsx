import React from 'react';

interface Props
{
    contentRef?,
    viewportRef?,
    thumbWidth?: string,

    scrollPos: number,
    onScroll(newScrollPos),
}
interface State
{
    hover: boolean,

    dragging: boolean,
    deltaPosition,
}

/**
 * @author Germán Martínez
 */
class ScrollArea extends React.Component<Props,State>
{
    state: State =
    {
        hover: false,

        dragging: false,
        deltaPosition: null,
    }
    /* @internal */
    private scrollContent: any = null;
    /* @internal */
    private scrollViewport: any = null;
    /* @internal */
    private scrollBarVertThumb: any = null;

    /* @internal */
    private thumbHeight = 0;
    /* @internal */
    private thumbPosition = 0;
    /* @internal */
    private hasVertScrollBar = false;

    render()
    {
        let thumbHeight = this.thumbHeight;
        let thumbPosition = this.thumbPosition;
        let vertScrollBarVisible = this.hasVertScrollBar && (this.state.hover || this.state.dragging);

        const contentStyle =
        {
            top: -this.props.scrollPos,
        }
        const scrollBarStyle =
        {
            opacity: vertScrollBarVisible ? 1 : 0,
            width: this.props.thumbWidth,
        };
        const scrollBarThumbStyle =
        {
            top: thumbPosition,
            height: thumbHeight
        }
        return (
<div
    ref={ (elem) =>
    {
        this.scrollViewport = elem;
        if(this.props.viewportRef) this.props.viewportRef(elem);
    }}
    className="scroll-area"
    onWheel={(e) =>
    {
        this.validateScrollPos(this.props.scrollPos + e.deltaY);
    }}
    onMouseOver={() =>
    {
        if(!this.state.hover)
        {
            this.setState({ hover: true });
        }
    }}
    onMouseEnter={() =>
    {
        this.setState({ hover: true });
    }}
    onMouseLeave={() =>
    {
        this.setState({ hover: false });
    }}
>
    <div
        ref={ (elem) =>
        {
            this.scrollContent = elem;
            if(this.props.contentRef) this.props.contentRef(elem);
        }}
        style={contentStyle}
        className="scroll-area-content"
    >
        { this.props.children }
    </div>
    <div
        className={ "scroll-bar" + (vertScrollBarVisible ? ' visible' : '')}
        style={scrollBarStyle}
        
        onMouseDown={(event) =>
        {
            this.startDrag(event);
        }}
    >
        <div
            ref={elem => this.scrollBarVertThumb = elem}
            className="scroll-bar-thumb"
            style={scrollBarThumbStyle}
        />
    </div>
</div>
        );
    }

    componentDidUpdate()
    {
        this.recalc();
    }

    componentDidMount()
    {
        window.addEventListener('resize', this.onResize);
        document.addEventListener('mousemove', this.onDrag);
        document.addEventListener('touchmove', this.onDrag);
        document.addEventListener('mouseup', this.stopDrag);
        document.addEventListener('touchend', this.stopDrag);
    }

    componentWillUnmount()
    {
        window.removeEventListener('resize', this.onResize);
        document.removeEventListener('mousemove', this.onDrag);
        document.removeEventListener('touchmove', this.onDrag);
        document.removeEventListener('mouseup', this.stopDrag);
        document.removeEventListener('touchend', this.stopDrag);
    }

    public scrollToBottom = () =>
    {
        this.props.onScroll(this.scrollContent.scrollHeight - this.scrollViewport.clientHeight);
    }

    private recalc = () =>
    {
        let thumbHeight = 0;
        let thumbPosition = 0;
        let hasVertScrollBar = false;
        if(this.scrollContent != null && this.scrollViewport != null)
        {
            const contentHeight = this.scrollContent.scrollHeight;
            const viewportHeight = this.scrollViewport.clientHeight;

            thumbHeight = this.calcThumbSize(contentHeight, viewportHeight);

            const pos = this.props.scrollPos;
            const height = contentHeight - viewportHeight;
            const percent = (height === 0) ? 0 : (pos / height);

            thumbPosition = Math.round((viewportHeight - thumbHeight) * percent);

            hasVertScrollBar = (contentHeight > viewportHeight);
        }

        if(this.thumbHeight != thumbHeight
            || this.thumbPosition != thumbPosition
            || this.hasVertScrollBar != hasVertScrollBar)
        {
            this.thumbHeight = thumbHeight;
            this.thumbPosition = thumbPosition;
            this.hasVertScrollBar = hasVertScrollBar;
            this.forceUpdate(() =>
            {
                this.validateScrollPos(this.props.scrollPos);
            });
        }
    }

    /* @internal */
    private onResize = (event) =>
    {
        this.validateScrollPos(this.props.scrollPos);
    }

    /* @internal */
    private onDrag = (event) =>
    {
        if(this.state.dragging)
        {
            event.preventDefault();
            event.stopPropagation();

            const e = event.changedTouches ? event.changedTouches[0] : event;

            const movement = e.clientY - this.state.deltaPosition;

            if(movement != 0)
            {
                const contentHeight = this.scrollContent.scrollHeight;
                const viewportHeight = this.scrollViewport.clientHeight;

                const height = contentHeight - viewportHeight;
                const thumbHeight = this.calcThumbSize(contentHeight, viewportHeight);

                const movementPercentage = movement/(viewportHeight - thumbHeight);
                const newScrollPos = movementPercentage*height;

                this.props.onScroll(newScrollPos);
            }
        }
    }

    /* @internal */
    private stopDrag = (event) =>
    {
        if(this.state.dragging)
        {
            this.setState({
                dragging: false
            });
        }
    }

    /* @internal */
    private startDrag = (event) =>
    {
        event.preventDefault();
        event.stopPropagation();

        const e = event.changedTouches ? event.changedTouches[0] : event;

        if(this.scrollBarVertThumb == e.target)
        {
            const contentHeight = this.scrollContent.scrollHeight;
            const viewportHeight = this.scrollViewport.clientHeight;
            const thumbHeight = this.calcThumbSize(contentHeight, viewportHeight);

            const pos = this.props.scrollPos;
            const height = contentHeight - viewportHeight;
            const percent = (height === 0) ? 0 : (pos / height);

            const thumbPosition = Math.round((viewportHeight - thumbHeight) * percent);
            const deltaPosition = e.clientY - thumbPosition;

            this.setState({
                dragging: true,
                deltaPosition,
            });
        }
    }

    /* @internal */
    private calcThumbSize = (contentSize, viewportSize) =>
    {
        if(contentSize == 0)
        {
            return 0;
        }
        return Math.max(20, viewportSize * (viewportSize / contentSize));
    }

    /* @internal */
    private validateScrollPos = (newScrollPos) =>
    {
        const contentHeight = this.scrollContent.scrollHeight;
        const viewportHeight = this.scrollViewport.clientHeight;

        if(newScrollPos > (contentHeight-viewportHeight))
        {
            newScrollPos = contentHeight-viewportHeight;
        }
        if(newScrollPos < 0)
        {
            newScrollPos = 0;
        }
        if(newScrollPos != this.props.scrollPos)
        {
            if(this.props.onScroll)
            {
                this.props.onScroll(newScrollPos);
            }
        }
    }
}
export default ScrollArea;
