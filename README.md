# Germix React Core - ScrollArea

## About

Germix react core scrollarea component

## Installation

```bash
npm install @germix/germix-react-core-scrollarea
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
